<?php

require_once 'composers.php';

Route::get('/photo', function(){
    $photo = new \Family\Image\Image();
    $photo->isPhoto();
});

#basic pages
Route::get('/', 'HomeController@hello');
Route::get('contact', 'HomeController@contact');
Route::post('contact', 'HomeController@send');

#scraping
Route::get('/scrape/next', 'ScrapeController@next');
Route::get('/scrape/towns', 'ScrapeController@town');//scrapes all towns in db
Route::get('/scrape/all', function(){
    \Queue::push('queueScrapeAll', array('location' => 'Hull'));
    \Queue::push('queueScrapeVenue');
});
Route::get('/scrape/{town}', 'ScrapeController@index');

//attractions routes
Route::post('/family-fun', 'AttractionsController@index');
Route::get('/family-fun', 'AttractionsController@index');
Route::get('/family-fun-in/{searchArea}', 'AttractionsController@index');
Route::get('/family-fun/{slug}', 'AttractionsController@show');
Route::post('/attractions/contact', 'AttractionsController@contact'); //for contact form
Route::resource('/attractions', 'AttractionsController');

//reviews
Route::post('/reviews', 'ReviewsController@store');


//used to get location via ajax
Route::get('/json/iplocation', 'JsonController@ipLocation');

//used to get map into modal
Route::post('/json/map', 'MapController@jsonShow');


Route::post('queue/index', function(){
    return Queue::marshal();
});

Route::get('/scrape/all', function(){
    \Queue::push('queueScrapeAll', array());
    \Queue::push('queueScrapeVenue');
});


class queueScrapeAll{
    public function fire($job, $data){
        $organiser = new \Family\Scraper\ScraperOrganiser();
        $today = date('Y-m-d 12:00:00');
        try {
            $nextTown = Town::where('scraped_at', '!=', $today)->firstOrFail();
        }catch (Exception $e){
            return false;
        }
        $nextTown->scraped_at = $today;

        #looks for pages based on town
        $organiser->searchWeb(strtolower($nextTown->town));
        $nextTown->save();
        //push again to loop until no new towns found today
        \Queue::push('queueScrapeAll', array());
    }
}

//this will request a queue to scrape town based on users input
class queueScrape{
    public function fire($job, $data){
       $organiser = new \Family\Scraper\ScraperOrganiser();
        $organiser->searchWeb($data["location"]);
    }
}

#this will start a loop of quese until database is scraped
class queueScrapeVenue{
    public function fire($job, $data){
        $organiser = new \Family\Scraper\ScraperOrganiser();

        //loop queue until all venues scraped
        if($organiser->nextPageToScrape()) Queue::push('queueScrapeVenue');

        $organiser->scrapeNextPage();
    }
}
