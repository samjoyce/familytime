{{Form::open(array('action' => 'ReviewsController@store', 'class' => 'small-12 columns'))}}
<div class="form-group">
    {{Form::label('reviewer', 'Your Name')}}
    {{Form::text('reviewer', null, array('placeholder' => 'enter your name'))}}
</div>
<div class="form-group">
    {{Form::label('reviewer_email', 'Your Email')}}
    {{Form::text('reviewer_email', null, array('placeholder' => 'enter your email'))}}
</div>
<div class="form-group">
    {{Form::label('headline', 'Headline')}}
    {{Form::text('headline', null, array('placeholder' => 'enter quick summary'))}}
</div>
<div class="form-group">
    {{Form::label('review', 'Review')}}
    {{Form::textarea('review', null, array('placeholder' => 'enter your review'))}}
</div>
<div class="form-group">
    {{Form::submit('Post Review', array('class' => 'btn'))}}
</div>
{{Form::hidden('attraction_id', $attraction->id)}}

{{Form::close()}}
