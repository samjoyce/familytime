<div class="review small-12 columns">
    <p><strong>{{$review->headline}}</strong></p>
    <small>{{$review->reviewer}} - Posted: {{$review->present()->created_at}}</small>
    <p>{{$review->review}}</p>
</div>