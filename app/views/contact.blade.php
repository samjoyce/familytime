@extends('layouts.fullWidth')

@section('content')
<div id="mainContent" class="row">
    <div class="row bigGap">
        <div class="small-12 medium-7 columns">
            <div id="description" class="panel big">
                <div class="flash">
                    <div class="innerRight"></div>
                    <h1>Contact Us</h1>
                </div>


                {{Form::open(array('url' =>'contact', 'class' => 'small-12 columns'))}}

                <div class="form-group">
                    {{Form::label('userName', 'Your Name')}}
                    {{Form::text('userName', null, array('placeholder' => 'enter your name'))}}
                </div>

                <div class="form-group">
                    {{Form::label('userEmail', 'Your Email')}}
                    {{Form::email('userEmail', null, array('placeholder' => 'enter your email address for reply'))}}
                </div>

                <div class="form-group">
                    {{Form::label('userMessage', 'Message')}}
                    {{Form::textarea('userMessage', null, array('placeholder' => 'enter your message'))}}
                </div>

                <div class="form-group">
                    {{Form::submit('send',array('class' => 'btn'))}}
                </div>

                {{Form::close()}}
            </div>
        </div>

    </div>
</div>
@stop
