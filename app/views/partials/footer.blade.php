<footer>
    <div class="row">
        <a id="footerClose" class="right">&#215;</a>

        @foreach($countiesBlock as $counties)
         <ul class="small-12 medium-3 columns">
            @foreach($counties as $county)
            <li>{{HTML::link('family-fun-in/'.$county, $countyLink[rand(0, (count($countyLink)-1))].$county)}}</li>
            @endforeach
        </ul>
        @endforeach

        <ul>
            <li>{{HTML::link('/contact', 'Contact Us')}}</li>

        </ul>
    </div>
    <p>© Elmhurst Web Services Ltd 2013 - <?php echo date('Y'); ?>. All rights reserved.</p>
</footer>