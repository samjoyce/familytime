<div class="panel">

    <div class="flash right title">
        <div class="innerLeft"></div>
        <h1>{{$attraction->name}}</h1>
    </div>

    <img src="{{$images->set($attraction->slug_uri)->getThumbnail($attraction->category_id)}}">


    <small>
        Distance: {{$attraction->present()->getDistanceFromUser}}
        - Category: {{$attraction->present()->category}}
    </small>

    <div class="description">{{$attraction->present()->excerpt}}</div>


  {{HTML::link('family-fun/'.$attraction->slug_uri, 'Details', array('class' => 'btn'))}}


</div>