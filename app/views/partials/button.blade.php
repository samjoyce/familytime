<a @if(isset($id)) id="{{$id}}" @endif class="flash right" href="{{$url}}">
    <div class="innerRight"></div>
    <h4>{{$value}}</h4>
</a>