<header>
    <div class="row">
        <div id="logo">
            <a href="{{url('/')}}">{{HTML::image('assets/images/logo.png')}}</a>
        </div>


        <h2 class="right hide-for-small-only">Fun for the whole family in the UK</h2>
    </div>
</header>