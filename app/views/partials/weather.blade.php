@for($x=0; $x<4; $x++)
<div class="weather">
    <strong>{{$location->weather->getDay($x)}}</strong>
    <img src="{{$location->weather->getIconUrl($x)}}"/>
    <strong>{{$location->weather->getConditions($x)}}</strong>

    <p>High: {{$location->weather->getHigh($x)}}C</p>

    <p>Low: {{$location->weather->getLow($x)}}C</p>
</div>
@endfor