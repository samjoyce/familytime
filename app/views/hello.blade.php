@extends('layouts.fullWidth')


@section('content')

<div class="row">
    <div id="home-search" class="small-12 medium-10 medium-offset-1 columns">
        <h1>Hello! Let the fun begin at ...</h1>
        {{Form::open(array('url' => '/family-fun', 'class' => 'small-12 columns no-gutter'))}}
        <div id="home-form">
            {{ Form::text('searchArea', $formatted_address, array('id' => 'searchArea', 'class' => 'form-control', 'placeholder' =>
            'Postcode, Town or County') )}}

            @if ($errors->has('searchArea'))
            <span class="help-block">{{ $errors->first('searchArea') }}</span>
            @endif

            {{Form::hidden('human', 'false', array('id' => 'human'))}}
            {{ Form::submit('Search', array('class' => 'btn'))}}
        </div>
    </div>

    {{Form::close()}}


    {{HTML::image('assets/images/family-fun-days.png', 'Family Fun days Out', array('class' => 'small-12 medium-8 medium-offset-2 columns left bigGap'))}}


    <a id="footerButton" href="#">&#x25BC</a>
</div>


@stop

@section('scripts')

<script>
    $(document).ready()
    {

        $('#human').val('true');

        if($('#searchArea').val().length == 0) {
            $.ajax({
                url: "json/iplocation",
                method: 'GET',
                dataType: 'json',
                success: function ($result) {
                   $('#searchArea').val($result.cityName + ', ' + $result.regionName);
                }
            });
        }

        $('#footerButton, #footerClose').click(function(){
            $('footer').toggle();
            $('#footerButton').toggle();
        });

        $('footer').hide();
        $('#footerButton, #footerClose').show();
    }
</script>
@stop