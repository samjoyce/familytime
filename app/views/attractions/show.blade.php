@extends('layouts.fullWidth')

@section('meta')
<title>Funbank.co.uk - {{$attraction->name}}</title>
<meta type="description" content="{{$attraction->present()->excerpt}}">
@stop


@section('content')

<div id="modalMap" class="reveal-modal" data-reveal>
    <h2>Location of {{$attraction->name}}</h2>

    <div id="modalMapContent">
    </div>
    <a class="close-reveal-modal">&#215;</a>
</div>

<div id="modalContact" class="reveal-modal" data-reveal>
    <h2>Contact {{$attraction->name}}</h2>

    <div>
        @include('attractions.contactForm')
    </div>
    <a class="close-reveal-modal">&#215;</a>
</div>


<div id="mainContent" class="row">

    <div class="row bigGap">
        <div class="small-12 medium-7 columns">
            <div id="description" class="panel big">
                <div class="flash">
                    <div class="innerRight"></div>
                    <h1>{{$attraction->name}}</h1>
                </div>


                <div>
                    <small class="gap">
                        Distance: {{$attraction->present()->getDistanceFromUser}}
                        - Category: {{$attraction->present()->category}}
                    </small>

                    {{$attraction->getDescriptionWithImages()}}
                </div>
            </div>

            <div id="reviews" class="panel big small-12 columns">
                <div class="flash">
                    <div class="innerRight"></div>
                    <h1>Reviews</h1>
                </div>

                @if(count($attraction->review) > 0)
                @foreach($attraction->review as $review)
                @include('reviews.review')
                @endforeach
                @else
                <strong class="gap">Sorry, there is no reviews for this attractions</strong>
                @endif

                <div class="row bigGap">
                    <p>Have you been here? Share your experience with others ...</p>
                    @include('reviews.form')
                </div>
            </div>

        </div>


        <div id="sidebar" class="small-12 medium-4 medium-offset-1 columns">

            <div class="panel side">
                <div class="flash">
                    <div class="innerRight"></div>
                    <h1>Weather</h1>
                </div>
                <div class="row clear">
                    @include('partials.weather')
                </div>
            </div>


            <div class="panel side">
                <div class="flash">
                    <div class="innerRight"></div>
                    <h1>Share</h1>
                </div>

                <div id="socialLinks" class="small-12 columns gap">
                    <a href="http://www.funbank.co.uk/family-fun/{{$attraction->slug_uri}}"
                       data-image="http://www.funbank.co.uk/{{$images->getThumbnail($attraction->category_id)}}"
                       data-title="{{$attraction->name}}"
                       data-desc="{{$attraction->present()->excerpt}}"
                       class="facebookShare">
                        {{HTML::image('assets/images/facebook-icon.png', 'Facebook share button')}}
                    </a>

                    <a href="https://twitter.com/intent/tweet">
                        {{HTML::image('assets/images/twitter-icon.png', 'Twitter share button')}}
                    </a>

                    <a href="https://plus.google.com/share?url=http://www.funbank.co.uk/family-fun/{{$attraction->slug_uri}}"
                            onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;"><img
                            />
                        {{HTML::image('assets/images/google-icon.jpg', 'Google share button')}}
                    </a>
                </div>
            </div>


            <div class="panel side">
                <div class="flash">
                    <div class="innerRight"></div>
                    <h1>Details</h1>
                </div>

                <p><strong>Address: </strong>{{$attraction->present()->address}}</p>

                <p><strong>Telephone: </strong>{{$attraction->telephone}}</p>

                <p><strong>Website: </strong>{{HTML::link($attraction->website, $attraction->website, array('target' => '_blank'))}}</p>

                @if(isset($attraction->email))
                {{HTML::link('#', 'Contact' ,array('id' => 'contactButton', 'class' => 'btn'))}}
                @endif

                {{HTML::link('#', 'Map' ,array('id' => 'mapButton', 'class' => 'btn'))}}

            </div>

            <div class="panel side">
                <div class="flash">
                    <div class="innerRight"></div>
                    <h1>Opening Times</h1>
                </div>
                <p>{{$attraction->present()->opening_times}}</p>
            </div>

            <div class="panel side">
                <div class="flash">
                    <div class="innerRight"></div>
                    <h1>Admission</h1>
                </div>
                <p>{{$attraction->present()->admission}}</p>
            </div>


        </div>


    </div>
</div>
@stop

@section('scripts')

<!-- Require extrenal scripts -->
<script src="http://maps.googleapis.com/maps/api/js?key=AIzaSyDY0kkJiTPVd2U7aTOAwhc9ySH6oHxOIYM&sensor=false"></script>
<script type="text/javascript" src="//platform.twitter.com/widgets.js"></script>

<script>
    $(document).ready()
    {
        $('#contactButton').click(function (e) {
            e.preventDefault();
            $('#modalContact').foundation('reveal', 'open');
        });

        //map modal
        var map;
        var mapLocation = new google.maps.LatLng(<?php echo $attraction->lat;?>, <?php echo $attraction->lng;?>);
        $('#mapButton').click(function (e) {
            e.preventDefault();
            if (!map) {
                var mapOptions = {
                    zoom: 14,
                    center: mapLocation,
                    mapTypeId: google.maps.MapTypeId.ROADMAP

                };
                map = new google.maps.Map(document.getElementById("modalMapContent"), mapOptions);
                marker = new google.maps.Marker({
                    map: map,
                    draggable: false,
                    animation: google.maps.Animation.DROP,
                    position: mapLocation
                });
            }

            $('#modalMap').foundation('reveal', 'open');
            //resize once modal opened
            $('#modalMap').on('opened', function () {
                google.maps.event.trigger(map, 'resize');
                map.setCenter(mapLocation);
            });
        });

    }
</script>


@stop


