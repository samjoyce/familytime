@extends('layouts.fullWidth')

@section('meta')
<title>Things to do in {{$requestedLocation}}</title>
<meta type="description" content="Fun things for the family and kids to do in and around {{$requestedLocation}}, no matter what the weather.">
@stop

@section('content')

<div id="mainContent" class="row">
    <div class="row gap">
           <h1>List of attractions</h1>
     </div>

    <div class="row">
        @if(count($attractions) > 0)
        @foreach($attractions as $attraction)
        @include('partials.panelWide', $attraction)
        @endforeach
        @else
        <div class="small-12  medium-8 medium-offset-2 columns left-align bigGap">
        <h1>Sorry, we have no attractions listed in this area.</h1>
        <p>What we will do, is search the internet for you and see what we can find, this may take a little while. Please try again in five minutes.</p>
            </div>
        @endif

    </div>
</div>
@stop