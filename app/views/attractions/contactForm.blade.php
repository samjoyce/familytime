{{Form::open(array('action' =>'AttractionsController@contact', 'class' => 'small-12 medium-8 medium-offset-2 columns'))}}

{{Form::hidden('attraction_id', $attraction->id)}}

<div class="form-group">
    {{Form::label('userName', 'Your Name')}}
    {{Form::text('userName', null, array('placeholder' => 'enter your name'))}}
</div>

<div class="form-group">
    {{Form::label('userEmail', 'Your Email')}}
    {{Form::email('userEmail', null, array('placeholder' => 'enter your email address for reply'))}}
</div>

<div class="form-group">
    {{Form::label('userMessage', 'Message')}}
    {{Form::textarea('userMessage', null, array('placeholder' => 'enter your message'))}}
</div>

<div class="form-group">
    {{Form::submit('send',array('class' => 'btn'))}}
</div>

{{Form::close()}}