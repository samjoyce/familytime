<!DOCTYPE html>

<html>
<head>
    <link rel="shortcut icon" href="{{url('assets/images/favicon.ico')}}"/>
    @yield('title')

    @yield('meta')

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="google-site-verification" content="ynHsKc4Kpa5d9pbjCfT6Z2x6c4nxgsKQPvnLvZAUZS4" />

    {{HTML::style('assets/css/styles.css')}}

    @yield('styles')
</head>


<body>
@include('partials.header')

<div id="wrapper">
    @include('partials.modal')

    @yield('master-content')
</div>

@include('partials.footer')


{{HTML::script("assets/js/main.js")}}

@yield('scripts')

<script>
    $(document).foundation();



    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-36631967-7', 'auto');
    ga('send', 'pageview');
</script>

</body>

</html>