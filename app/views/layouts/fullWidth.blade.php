@extends('layouts.master')

@section('master-content')

@if(isset($flash))
    {{displayAlert($flash)}}
@endif
@if(Session::has('tellUser'))
    {{displayAlert((string)Session::get('tellUser'))}}
@endif

@yield('content')

@stop