<?php



class scraperDayOutWithTheKidsTest extends TestCase
{

    public function setup()
    {
        $this->scraper = new \Family\Scraper\scraperDayOutWithTheKids(new \AnthonyMartin\GeoLocation\GeoLocation());
        $this->page = "app/tests/Family/Scraper/fileToScrape.html";
    }

    /**
     * @group
     */
    public function test_can_get_the_venue_name()
    {
        $this->scraper->set($this->page);
        $name = $this->scraper->getName();
        $this->assertEquals($name, 'The British Wildlife Centre');
    }

    /**
     * @group
     */
    public function test_can_get_the_address()
    {
        $this->scraper->set($this->page);
        $name = $this->scraper->getAddress();
        $this->assertEquals(array("Eastbourne Road",
            "Newchapel",
            "Lingfield",
            "Surrey",
            "RH7 6LF"), $name);
    }

    /**
     * @group
     */
    public function test_can_get_the_telephone()
    {
        $this->scraper->set($this->page);
        $tel = $this->scraper->getTelephone();
        $this->assertEquals('01342 834658', $tel);
    }

    /**
     * @group
     */
    public function test_can_get_the_email()
    {
        $this->scraper->set($this->page);
        $email = $this->scraper->getEmail();
        $this->assertEquals('info@britishwildlifecentre.co.uk', $email);
    }

    /**
     * @group
     */
    public function test_can_get_the_website()
    {
        $this->scraper->set($this->page);
        $website = $this->scraper->getWebsite();
        $this->assertEquals('http://www.britishwildlifecentre.co.uk/', $website);
    }


    /**
     * @group
     */
    public function test_can_get_the_opening_times()
    {
        $this->scraper->set($this->page);
        $times = $this->scraper->getOpeningTimes();
        $this->assertEquals('2014 Opening Details:
The British Wildlife Centre opens every weekend and public holiday (excluding 25/26 Dec) AND during state school holidays throughout the year, as shown below:
2014 HOLIDAY OPENING DATES:
Easter:
Saturday 5 May to Monday 21 April : open daily
Summer Half Term:
Saturday 24 May to Sunday 1 June : open daily
Summer:
Thursday 24 July to Tuesday 2 September: open daily
Autumn Half Term:
Saturday 25 October to Sunday 2 November : open daily
Christmas:
Saturday 27 December to Sunday 4 Jan 2015 : open daily
Opening times: 10 am to 5 pm (Nov-Jan: 10am to 4pm)
Last admission: One hour before closing.', $times);
    }



    /**
     * @group
     */
    public function test_can_get_the_description()
    {
        $this->scraper->set($this->page);
        $desc = $this->scraper->getdescription();
        $this->assertEquals('The British Wildlife Centre is a peaceful, relaxing place to see and learn about Britains wonderful native wildlife from tiny harvest mice to magnificent red deer.http://www.britishwildlifecentre.co.uk/', $desc);
    }


    public function test_makes_slug_from_name(){
        $this->scraper->set($this->page);
        $slug = $this->scraper->getSlug();
        $this->assertEquals('the-british-wildlife-centre', $slug);

    }
}