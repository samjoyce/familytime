<?php
namespace Family\Image;

class RemoteImagesTest extends \TestCase
{


    public function setup()
    {
        $this->remoteImages = new RemoteImages();
        $this->website = "http://www.cedarhomes.org.uk";
        $this->slug = "cedarhomes";
        $this->imageDir = "images/";
        $this->imageToFind = "http://www.cedarhomes.org.uk/index-elizabeth.jpg";
    }

    public function tearDown()
    {

        $dirname = $this->imageDir . $this->slug;
        if (is_dir($dirname))
            $dir_handle = opendir($dirname);
        if (!$dir_handle)
            return false;
        while ($file = readdir($dir_handle)) {
            if ($file != "." && $file != "..") {
                if (!is_dir($dirname . "/" . $file))
                    unlink($dirname . "/" . $file);
                else
                    delete_directory($dirname . '/' . $file);
            }
        }
        closedir($dir_handle);
        rmdir($dirname);

    }


    /**
     * @group scrape
     */
    public function test_new_directory_is_setup_for_the_images()
    {
        $this->remoteImages->set($this->website, $this->slug);
        $this->assertFileExists($this->imageDir . $this->slug . '/');
    }

    /**
     * @group scrape
     */
    public function test_page_is_loaded()
    {
        $this->remoteImages->set($this->website, $this->slug);
        $this->assertInstanceOf('DOMDocument', $this->remoteImages->doc);
    }

    /**
     * @group scrape
     */
    public function test_can_open_remote_image_file()
    {
        $read = $this->remoteImages->set($this->website, $this->slug)->checkImageReadable($this->imageToFind);
        $this->assertEquals($read, true);
    }

    /**
     * @group scrape
     */
    public function test_can_save_file_if_large_enough_and_is_a_photo()
    {
        $this->remoteImages->set($this->website, $this->slug)->getPhoto($this->imageToFind, 1);
        $this->assertFileExists($this->imageDir . $this->slug . '/' . $this->slug . '-1.jpg');
    }



}