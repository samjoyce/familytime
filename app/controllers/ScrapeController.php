<?php

use Family\Scraper\ScraperOrganiser;

class ScrapeController extends BaseController
{


    public function __construct()
    {
        $this->orgainiser = new ScraperOrganiser();
    }


    /**
     * Scrapes the town in given location
     * @param $location
     * @return string
     */
    public function index($location)
    {
        #looks for pages based on town
        $this->orgainiser->searchWeb($location);

        return "SCRAPED";
    }

    /***
     * Scarpes the next individual attraction
     * @return string|void
     */
    public function next()
    {
        $scrape = $this->orgainiser->scrapeNextPage();

        return ($scrape) ? dd($scrape) : "could not scrape:" . $this->orgainiser->nextPageToScrape();
    }


    /**
     * Scrapes all towns listed in db that not scraped today
     */
    public function town()
    {
        $today = date('Y-m-d 12:00:00');
        $nextTown = Town::where('scraped_at', '!=', $today)->firstOrFail();
        $nextTown->scraped_at = $today;

        #looks for pages based on town
        $this->orgainiser->searchWeb(strtolower($nextTown->town));
        $nextTown->save();


        return $nextTown->town;
    }
}