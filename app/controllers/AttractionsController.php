<?php

use AnthonyMartin\GeoLocation\GeoLocation as GeoLocation;

class AttractionsController extends \BaseController
{

    public function __construct(\Family\Email\EmailInterface $email)
    {
        $this->email = $email;
        $this->imageRepo = new \Family\Image\ImageRepository();
        $this->location = new \Family\Location\Location();
    }


    public function index()
    {
        //add UK to request if GET HTTP
       $location = \Request::isMethod('POST') ? \Request::get('searchArea') : \Request::segment(2);
        $location .= ' UK';
        //if hidden human verify is human otherwise we can conserve api calls
       $conserveResource = (\Input::get('human')=='false') ? true : false;

       $setLocation = $this->location->setLocation($location, $conserveResource);

       if(! $setLocation) \Redirect::to('/')->with('flash', 'Sorry, your location could not be found');

       $attractions = Attraction::getNearBy($this->location->getLatitude(), $this->location->getLongitude());
       return View::make('attractions.index')
           ->with('attractions', $attractions)
           ->with('images', $this->imageRepo)
           ->with('requestedLocation', $location);
    }

    /**
     * Show the form for creating a new resource.
     * GET /attractions/create
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     * POST /attractions
     *
     * @return Response
     */
    public
    function store()
    {
        //
    }

    /**
     * Display the specified resource.
     * GET /attractions/{id}
     *
     * @param  int $id
     * @return Response
     */
    public function show($slug)
    {
       $attraction = Attraction::where('slug_uri', $slug)->firstOrFail();
       $images = $this->imageRepo->set($attraction->slug_uri);
       return View::make('attractions.show')
            ->with('attraction', $attraction)
            ->with('images', $images);
    }

    /**
     * Show the form for editing the specified resource.
     * GET /attractions/{id}/edit
     *
     * @param  int $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     * PUT /attractions/{id}
     *
     * @param  int $id
     * @return Response
     */
    public function update($id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * DELETE /attractions/{id}
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }

    public function contact(){
        $attraction = \Attraction::find(Input::get('attraction_id'));
        $this->email->send('emails.incoming', Input::all(), array(
            'to' => $attraction->email,
            'subject' => 'Message from a FunBank.co.uk user',
            'from' => \Input::get('userEmail')), false);

        return \Redirect::back()->with('tellUser', 'Success|Your Email has been sent!');
    }
}