<?php

use Family\Location;

class HomeController extends BaseController
{


    public function __construct(\Family\Email\EmailInterface $email)
    {
        $this->email = $email;
    }


    public function hello()
    {
        return View::make('hello');
    }


    public function contact()
    {
        return View::make('contact');
    }

    public function send()
    {
        $this->email->send('emails.incoming', Input::all(), array(
            'to' => \Config::get('admin.email'),
            'subject' => 'Message from FunBank.co.uk user',
            'from' => \Input::get('userEmail')), false);

        return View::make('contact')->with('flash', 'Success|Your Email has been sent!');
    }
}

