<?php

class Review extends \Eloquent {
	protected $guarded = ['id'];
    protected $presenter = 'Family\Presenters\ReviewPresenter';


    public function attraction(){
        return $this->belongsToOne('Attraction');
    }

    public function present(){
       return new $this->presenter($this);
    }
}