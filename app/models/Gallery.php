<?php

class Gallery extends \Eloquent {

    protected $guarded = ['id'];

    public function attraction(){
        return $this->belongsTo('Attraction');
    }
}