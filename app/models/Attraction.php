<?php

use AnthonyMartin\GeoLocation\GeoLocation;
use Family\Location\Location;
use Family\Presenters\AttractionsPresenter;


class Attraction extends \Eloquent
{

    protected $guarded = ['id'];
    protected $userLocation;
    protected $image;
    protected $presenter = 'Family\Presenters\AttractionsPresenter';

    public function __construct($attributes = array())
    {
        parent::__construct($attributes);
        $this->userLocation = new Location;
        $this->image = new \Family\Image\ImageRepository;
    }

    public function review()
    {
        return $this->hasMany('Review');
    }

    //create new instance for displaying items
    public function present()
    {
        return new $this->presenter($this);
    }

    public function gallery()
    {
        return $this->hasOne('Gallery');
    }

    static public function getNearBy($lat, $lng, $miles = 15)
    {
        $boundry = GeoLocation::fromDegrees($lat, $lng);
        $coordinates = $boundry->boundingCoordinates($miles, 'miles');

        return self::where('lat', '>', $coordinates[0]->getLatitudeInDegrees())
            ->where('lat', '<', $coordinates[1]->getLatitudeInDegrees())
            ->where('lng', '<', $coordinates[1]->getLongitudeInDegrees())
            ->where('lng', '>', $coordinates[0]->getLongitudeInDegrees())
            ->get();
    }

    public function getDistanceFromUser()
    {
        $longitude = $this->userLocation->getLongitude();
        $latitude = $this->userLocation->getLatitude();
        $currentLocation = GeoLocation::fromDegrees($latitude, $longitude);
        $attractionLocation = GeoLocation::fromDegrees($this->lat, $this->lng);

        return $attractionLocation->distanceTo($currentLocation, 'miles');
    }


    public function getDescriptionWithImages()
    {
        $article = new DOMDocument();
        @$article->loadHTML($this->description);
        $paragraphs = $article->getElementsByTagName('p');

        $this->image->set($this->slug_uri);
        $images = $this->image->getAllFilenames();

        $x = 1;//add image everytime this counter reaches three(every third p tag)
        $imageNumber = 0; //different image form array
        $float = 'right'; //position of image

        foreach ($paragraphs as $paragraph) {

            //if image is available add image tg to pargraph
            if(isset($images[$x])) {
                $addimage = ($x == 3) ? '<img class="' . $float . '" src="' . \Config::get('family.public_uri') . 'images/' . $this->slug_uri . '/' . $images[$imageNumber] . '"/>' : "";
            }else{
                $addimage = "";
            }
            echo '<p>' . $addimage . $paragraph->nodeValue . '</p>';

            if ($x > 3) {
                $x = 1;
                $imageNumber++;
                $float = ($float == 'right') ? 'left' : 'right';
            } else {
                $x++;
            }
        }
    }
}