<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddSlugUriToAttractionsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('attractions', function(Blueprint $table)
		{
			$table->string('slug_uri');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('attractions', function(Blueprint $table)
		{
			$table->dropColumn('slug_uri');
		});
	}

}
