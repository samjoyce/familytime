<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateAttractionsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('attractions', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('name');
			$table->text('description')->nullable();
			$table->string('address0');
			$table->string('address1');
			$table->string('address2');
			$table->string('address3')->nullable();
			$table->string('address4')->nullable();
			$table->string('address5')->nullable();
			$table->string('postcode')->nullable();
			$table->string('telephone')->nullable();
			$table->string('email')->nullable();
			$table->string('website')->nullable();
			$table->float('lat');
			$table->float('lng');
			$table->text('opening_times')->nullable();
			$table->text('admission')->nullable();
			$table->string('logo')->nullable();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('attractions');
	}

}
