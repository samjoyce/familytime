<?php


class TownsTableSeeder extends Seeder {

	public function run()
	{



		foreach(\Config::get('towns') as $town)
		{
			Town::create([
                'town' => $town
			]);
		}
	}

}