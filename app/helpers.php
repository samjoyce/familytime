<?php
/**
 * Displays an alert box in right format if sent
 * @return string
 */
function displayAlert($flash)
{
    list($type, $message) = explode('|', $flash);
    switch (strtolower($type)) {
        case 'error' :
            $type = 'alert';
            break;
        case 'message' :
            $type = 'secondary';
            break;
        case 'success' :
            $type = 'success';
            break;
    }
    return sprintf('<div class="alert-box %s"><div class="row">%s<a href="#" class="close">&times;</a></div></div>', $type, $message);

    return '';
}