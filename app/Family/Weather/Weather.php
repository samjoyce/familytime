<?php

namespace Family\Weather;

class Weather{

    protected $apiKey = '4b7126b5f78f555d';
    public $weather;

    /**
     * Get the weather json from the wunderground service
     * @param $location
     */
    public function setWeather($lat, $lng){
        $location = $this->prepForUrl($lat, $lng);
        $json_string = file_get_contents("http://api.wunderground.com/api/$this->apiKey/forecast10day/geolookup/conditions/q/$location.json");
        $this->weather = json_decode($json_string);
        return $this;
    }

    /**
     * Prepare given location to an acceptable format for api
     * @param $location
     */
    public function prepForUrl($lat, $lng){
        return $lat .',' . $lng;
    }

    /**
     * Show the whole json object
     */
    public function dumpJson(){
        var_dump($this->weather);
        var_dump($this->weather->forecast->simpleforecast->forecastday[0]);
    }

    /**
     * Expands the unit to required string format
     * @param $unit
     */
    protected function unitExpand($unit){
       if($unit == 'C') return 'celsius';
       if($unit == 'F') return 'fahrenheit';

    }


    /**
     * Get Highest Temperature
     * @param int $day
     * @param string $unitRequested
     * @return mixed
     */
    public function getHigh($day = 0, $unitRequested = 'C'){
        $unit = $this->unitExpand($unitRequested);
        return $this->weather->forecast->simpleforecast->forecastday[$day]->high->{$unit};
    }


    /**
     * Get Lowest Temperature
     * @param int $day
     * @param string $unitRequested
     * @return mixed
     */
    public function getLow($day = 0, $unitRequested = 'C'){
        $unit = $this->unitExpand($unitRequested);
        return $this->weather->forecast->simpleforecast->forecastday[$day]->low->{$unit};
    }

    /**
     * Get conditions
     * @param $day
     * @return mixed
     */
    public function getConditions($day = 0){
        return $this->weather->forecast->simpleforecast->forecastday[$day]->conditions;
    }

    /**
     * Get icon
     * @param $day
     * @return mixed
     */
    public function getIcon($day = 0){
        return $this->weather->forecast->simpleforecast->forecastday[$day]->icon;
    }


    /**
     * Get day
     * @param $day
     * @return mixed
     */
    public function getDay($day = 0){
       return $this->weather->forecast->simpleforecast->forecastday[$day]->date->weekday_short;
    }


    /**
     * Get icon url from cdn
     * @param $day
     * @return mixed
     */
    public function getIconUrl($day = 0){
        return $this->weather->forecast->simpleforecast->forecastday[$day]->icon_url;
    }


    /**
     * Get custom field from the json
     * @param $day
     * @return mixed
     */
    public function getField($field, $day = 0){
        $property = $this->weather->forecast->simpleforecast->forecastday[$day];

        return property_exists($property, $field) ? $property->{$field} : null;
    }
}