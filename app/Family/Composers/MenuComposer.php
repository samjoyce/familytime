<?php

namespace Family\Composers;

class MenuComposer
{


    public function compose($view)
    {
        $allCounties = \Config::get('menu.counties');
        $countiesBlock = array_chunk($allCounties, count($allCounties) / 3);
        $countyLink = \Config::get('menu.countyLink');
         $view->with('countiesBlock', $countiesBlock)
         ->with('countyLink', $countyLink);
    }
}