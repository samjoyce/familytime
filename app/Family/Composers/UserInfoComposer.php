<?php

namespace Family\Composers;

use Family\Location\Location;

class UserInfoComposer{

    public function __construct(){
        $this->location = new Location;
    }

    public function compose($view){
        $location = $this->location->getLocation();

        $formatted_address = isset($location->results[0]->formatted_address) ? $location->results[0]->formatted_address : null;

        $view->with('formatted_address', $formatted_address);
    }
}