<?php
namespace Family\Image;


class Image extends \Intervention\Image\Image
{

    protected $minPhotoSize = 150;

    #get qty of colours
    public function qtyColors()
    {
        return imagecolorstotal($this->resource);
    }

    #is image a photo based on the size and number of colors of a box scan in the middle of the image
    public function isPhoto($pixels = 100)
    {
        $colors = array();

        //if image is to small to scan
        if($this->width < $pixels || $this->height < $pixels) return false;

        $boxStart_x = ceil(($this->width / 2) - ($pixels / 2));
        $boxStart_y = ceil(($this->height / 2) - ($pixels / 2));

        //scan through box of $pixels wide and high and add colors to array for counting
        for ($y = $boxStart_y; $y < ($boxStart_y + $pixels); $y++) {
            for($x = $boxStart_x; $x < ($boxStart_x + $pixels); $x++) {
                 $pixelColor = imagecolorat($this->resource, $x, $y);
                if (array_key_exists($pixelColor, $colors)) {
                    $colors[$pixelColor]++;
                } else {
                    $colors[$pixelColor] = 1;
                }
            }
        }

        $ratio = array_sum($colors) / count($colors); //the average pixels per color
        $isPhoto = (max($colors) < ($ratio * 30)) ? true : false; //if dominant color far exceeds average (by 30 time) then its not photo

        return $isPhoto;

    }

} 