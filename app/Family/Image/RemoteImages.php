<?php
namespace Family\Image;


/**
 * This class grabs the images of a website and sends to an image repo
 */

use Intervention\Image\Exception\InvalidImageDataStringException;

class RemoteImages
{
    public function __construct()
    {
        $this->imager = new Image();
        $this->repository = new ImageRepository();
    }


    public function set($website, $slug)
    {
        $this->website = $website;
        $this->slug = $slug;
        $this->repository->set($this->slug);

        $this->doc = new \DOMDocument();
        $this->imageGrabbed = array(); #array of images grabbed to prevent repeat
        return $this;
    }

    /**
     * go through DOM and get all the images and save them
     */
    public function getAllImages()
    {
        $pages = $this->getPagesWithPhotosFromWebsite();

        //$pages = false;

        if(!$pages) return false;

        foreach ($pages as $page) {
            echo "<br />page to scrape: " . $page;
            $imageNumber = 0;
            if ($this->checkUrlReadable($page)) {
                echo '<b>SCRAPED</b>';
                $html = file_get_contents($page);
                @$this->doc->loadHTML($html);
                $images = $this->doc->getElementsByTagName('img');
                foreach ($images as $img) {
                    $photoURL = $this->validateURL($img->getAttribute('src'));

                    if ($this->checkUrlReadable($photoURL) && !in_array($photoURL, $this->imageGrabbed)) {
                        $photo = $this->getPhoto($photoURL);
                        if ($photo) {
                            $this->repository->save($photo, $this->slug);
                            array_push($this->imageGrabbed, $photoURL);
                            $imageNumber++;
                        }
                    }
                }
            }
            if($imageNumber > 9) break; #exit if too many images
        }
        $this->repository->setFeatureImage();
        return true;
    }

    /**
     * check can read remote image
     * @param $photoURL
     * @return bool
     */
    public function checkUrlReadable($URL)
    {
        try {
            fopen($URL, 'r');
            return true;
        } catch (\ErrorException $e) {
            return false;
        }
    }

    /**
     * grab the photo
     * @param $photoURL
     * @param $imageNumber
     */
    public function getPhoto($photoURL)
    {
        try {
            $photo = $this->imager->make($photoURL);
        } catch (InvalidImageDataStringException $e) {
            return false;
        }

        return $photo->isPhoto() ? $photo : false;
    }


    /**
     * returns an array of pages to scrape from a website
     */
    public function getPagesWithPhotosFromWebsite()
    {
        $listOfPages = array($this->website);

        if( ! $this->checkUrlReadable($this->website)) return false;

        $dom = file_get_contents($this->website);

        @$this->doc->loadHTML($dom);

        $pages = $this->doc->getElementsByTagName('a');
        foreach ($pages as $page) {
            if ($this->linkIsRequired($page)) {
                array_push($listOfPages, $this->validateURL($page->getAttribute('href')));
            }
        }


        return array_slice($listOfPages, 0, 5);
    }


    /**
     * check the link is what is wanted based on the current method
     * must be in array in link, anchor, not have a hash and must have venue web address(no
     * outside link, ie facebook)
     * @param $link
     * @return bool
     */
    protected function linkIsRequired($link)
    {
        $keys = array('about', 'gallery', 'photos');
        foreach ($keys as $key) {

            if (strpos($link->nodeValue, $key) != false || strpos($link->getAttribute('href'), $key) != false) {
                return !strpos($link->getAttribute('href'), '#') ? true : false;
            }
        }

        return false;
    }


    /**
     * Make url valide if it doesn't have full path
     * @param $url
     * @return string
     */
    protected function validateURL($url)
    {
        $url = ltrim($url, '/');
        return (!strpos($url, 'ttp://') == 0) ? $url : rtrim($this->website, "/") . '/' . $url;

    }

}


