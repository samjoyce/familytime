<?php
namespace Family\Image;

use Intervention\Image\Image as VendorImage;

/**
 * Saves and organises the images for the venues
 * Class ImageRepository
 * @package Family\Image
 */
class ImageRepository
{
    protected $path; //path to image files

    protected $dir; // directory for a venue

    public function __construct()
    {
        $this->path = 'images/';
        $this->image = new VendorImage();
    }

    public function set($slug)
    {
        $this->dir = $this->path . $slug . '/';
        $this->slug = $slug;
        is_dir($this->dir) ?: mkdir($this->dir);
        return $this;
    }

    /**
     * return array of all image files in diectory
     * @return array
     */
    public function getAllFilenames()
    {
        $images = array();
        $files = scandir($this->dir);
        foreach ($files as $file) {
            if (!is_dir($file)) array_push($images, $file);
        }
        return $images;
    }

    /**
     * Get the next file name to save a image
     * @param $slug
     * @return string
     *
     */
    public function getNextFilename($slug)
    {
        $images = 0;
        if ($handle = opendir($this->dir)) {
            while (($file = readdir($handle)) !== false) {
                if (!in_array($file, array('.', '..')) && !is_dir($this->dir . $file))
                    $images++;
            }
        }
        return $this->dir . $slug . '-' . $images . '.jpg';
    }

    /**
     * Save the image
     * @param Image $photo
     * @param $slug
     */
    public function save(Image $photo, $slug)
    {
        $photo->save($this->getNextFilename($slug));
    }


    /**
     * Return best option for thmbnail
     * @return bool|string
     */
    public function getThumbnail($category)
    {
        $filename = $this->dir . $this->slug . '-featured.jpg';
        if (file_exists($filename)) return $filename;
        # if no featured image, find something else;
        switch ($category) {
            case 0:
                return public_uri() . '/assets/images/funbank-active.jpg';
                break;
            case 1:
                return public_uri() . '/assets/images/funbank-playcentre.jpg';
                break;
            case 2:
                return public_uri() . '/assets/images/funbank-animals.jpg';
                break;
            case 3:
                return public_uri() . '/assets/images/funbank-educational.jpg';
                break;
            case 4:
                return public_uri() . '/assets/images/funbank-water.jpg';
                break;
            case 7:
                return public_uri() . '/assets/images/funbank-creative.jpg';
                break;
        }
        return public_uri() . '/assets/images/default-fun.jpg';
    }

    /**
     * Sort out which scraped image should be featured Image
     */
    public function setFeatureImage()
    {
        $images = $this->getAllFilenames();
        foreach ($images as $image) {
            $size = getimagesize($this->dir . $image);
            if ($this->landscaped($size) && $size[0] > 180) #if well landscaped
            {
                $this->image->open($this->dir . $image)
                    ->resize('400', null, true)
                    ->crop('400', '150')
                    ->save($this->dir . $this->slug . '-featured.jpg');

                return true;
            }
        }

    }

    /**
     * @param $size
     * @return boolreturn is landscaped but not panoramic
     */
    protected function landscaped($size)
    {
        return ($size[0] >= $size[1] && $size[0] < ($size[1] * 2.3)) ? true : false;
    }
} 