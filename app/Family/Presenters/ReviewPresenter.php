<?php
namespace Family\Presenters;

class ReviewPresenter extends BasePresenter{

    /**
     * Headline presnter to make a sentence not exceeding $characters
     * @return mixed
     */
    public function created_at()
    {
       return \Carbon\Carbon::createFromTimeStamp(strtotime($this->entity->created_at))->diffForHumans();
    }


}