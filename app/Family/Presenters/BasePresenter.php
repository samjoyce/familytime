<?php

namespace Family\Presenters;

class BasePresenter{
    /**
     * Inject the object to be presented
     * @param $entity
     */
    public function __construct($entity)
    {
        $this->entity = $entity;
    }

    /**
     * find the method if property had a presneter method set else return the original property
     * @param $property
     * @return mixed
     */
    public function __get($property)
    {
        if (method_exists($this, $property)) {
            return $this->{$property}();
        }
        return $this->entity->{$property};
    }


}