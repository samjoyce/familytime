<?php
namespace Family\Presenters;

class AttractionsPresenter extends BasePresenter
{

    /**
     * Headline presnter to make a sentence not exceeding $characters
     * @return mixed
     */
    public function excerpt()
    {
        $excerpt = $this->entity->description;
        $excerpt = substr($excerpt, 0, strpos($excerpt, "</p>")); //cut all but first paragraph
        $excerpt = substr($excerpt, 0, 200);
        $excerpt = substr($excerpt, 0, strrpos($excerpt, " ")); //remove last word as it may not be whole
        if(strlen($excerpt) > 5) return $excerpt . '...';
    }


    public function getDistanceFromUser()
    {
        return ceil($this->entity->getDistanceFromUser()) . ' miles';
    }

    public function opening_times()
    {
        return nl2br($this->entity->opening_times);
    }

    public function admission()
    {
        return nl2br($this->entity->admission);
    }

    //present the address for the sidde bar
    public function address()
    {
        $address = !isset($this->entity->address1) ? : $this->entity->address1;
        $address .= !isset($this->entity->address2) ? : ', ' . $this->entity->address2;
        $address .= !isset($this->entity->address3) ? : ', ' . $this->entity->address3;
        $address .= !isset($this->entity->address4) ? : ', ' . $this->entity->address4;

        return $address;
    }

    public function category()
    {
        $categories = \Config::get('categories');

        return isset($this->entity->category_id)
            ? $categories[$this->entity->category_id]
            : 'Not categorized';
    }

}