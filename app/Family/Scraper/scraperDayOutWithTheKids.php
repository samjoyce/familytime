<?php

namespace Family\Scraper;

use AnthonyMartin\GeoLocation\GeoLocation;
use Family\Image\Image;
use Family\Image\RemoteImages;
use samjoyce\slugger\Slugger;
use Symfony\Component\DomCrawler\Crawler;


class scraperDayOutWithTheKids implements scraperInterface
{

    public $address = array(null, null, null, null, null);

    public function __construct()
    {
        $this->crawler = new Crawler();
        $this->venueWebsiteCrawler = new Crawler();
        $this->geo = new GeoLocation();
        $this->slugger = new Slugger(\DB::connection()->getPdo());
        $this->imager = new Image();
        $this->photoGrabber = new RemoteImages();
    }

    /**
     * Set the instance to the requested html page
     * @param $page
     */
    public function set($page)
    {
        try {
            $this->crawler->addContent(file_get_contents($page));
        }
        catch (\ErrorException $e){
            return false;
        }
        return $this;
    }

    /**
     * Get the contact details to be used to get name, address and telephone
     * @return array
     */
    public function contactDetails()
    {
        #grab the address box
        $details = $this->crawler->filter('div.box1')->html();

        if ($details == null) return null;

        #strip out telephone bit
        $details = str_replace('<span class="highlight">Telephone: </span>', '', $details);

        $result = explode("<br>", $details);

        $contact = array();
        #remove whitespace
        foreach ($result as $str) {
            array_push($contact, trim($str));
        }

        return $contact;
    }

    public function getAllVenueData()
    {
        $this->address = $this->getAddress();

        $this->name = $this->clean($this->getName());

        $this->telephone = $this->getTelephone();

        $this->email = $this->getEmail();

        $this->description = $this->getDescription();

        $this->website = $this->getWebsite();

        $this->admission =$this->clean($this->getAdmission());

        $this->openingTimes = $this->clean($this->getOpeningTimes());

        $this->logo = $this->getLogo($this->website);

        $this->geo = $this->getGeo();

        $this->slug = $this->getSlug();

        $this->getPhotos($this->website, $this->slug);

        return $this;
    }

    public function asArray()
    {
        return array("name" => $this->name,
            "description" => $this->description,
            "website" => $this->website,
            "email" => $this->email,
            "telephone" => $this->telephone,
            "admission" => $this->admission,
            "opening_times" => $this->openingTimes,
            "logo" => $this->logo,
            "address0" => $this->address[0],
            "address1" => $this->address[1],
            "address2" => $this->address[2],
            "address3" => isset($this->address[3]) ? $this->address[3] : null,
            "address4" => isset($this->address[4]) ? $this->address[4] : null,
            "lat" => isset($this->geo->lat) ? $this->geo->lat : 0,
            "lng" => isset($this->geo->lng) ? $this->geo->lng : 0,
            "slug_uri" => $this->slug,
        );
    }

    /**
     * @return array
     */
    public function getAddress()
    {
        $details = $this->contactDetails();
        array_shift($details); #remove the name
        if (preg_match('/^[0-9 ]+$/i', end($details))) { #remove telepone number
            array_pop($details);
        };
        $details[4] = isset($details[4]) ? $details[4] : null;
        return $details;
    }

    /**
     * gets the venue name from the contct detals and removes
     * @param $contact
     * @return mixed
     */
    public function getName()
    {
        $details = $this->contactDetails();
        return array_shift($details);
    }

    /**
     * @param $contact
     */
    public function getTelephone()
    {
        $details = $this->contactDetails();
        if (preg_match('/^[0-9 ]+$/i', end($details))) {
            return array_pop($details);
        };

        return null;
    }

    /**
     *
     */
    public function getEmail()
    {
        $email = $this->crawler->filter('div.email > a')->attr('href');
        if (strpos($email, 'to:') == 0) return null;

        $email = str_replace('?subject=An enquiry from DayOutWithTheKids.co.uk', '', $email);
        $email = str_replace('mailto:', '', $email);
        return $email;
    }

    /**
     * @return array
     */
    public function getDescription()
    {
        $blurb = $this->scanDescription();

        if(strpos('www.', end($blurb)) != -1) array_pop($blurb); #remove last array ele if just web addess

        for($x = 0; $x < count($blurb); $x++){
            $blurb[$x] = '<p>' . $blurb[$x] . '</p>';
        }
        return implode('', $blurb);
    }

    /**
     * return website if last item in description
     * @param $description
     * @return string
     */
    public function getWebsite()
    {
        $description = $this->scanDescription();
        return strpos(end($description), 'www.') ? array_pop($description) : null;
    }

    /**
     * @return null|string
     */
    public function getAdmission()
    {
        $admission = $this->crawler->filter('div.box3 > span.colR');

        return count($admission) ? $admission->first()->text() : null;
    }

    /**
     * @return null|string
     */
    public function getOpeningTimes()
    {
        $opening = $this->crawler->filter('div.box3 > span.colR');

        return count($opening) ? $opening->last()->text() : null;
    }


    /**
     * Gets the url address of first image on venues website
     * @param $website
     * @return null|string
     */
    public function getLogo($website)
    {
        if (!$website) return null;

        try {
            $html = file_get_contents($website);
        }catch (\ErrorException $e){
            return null;
        }

        $this->venueWebsiteCrawler->addContent($html);

        $image = $this->venueWebsiteCrawler->filter('img');

        if(count($image)){
            $image = $image->first()->attr('src');
            return strpos($image, 'www.') ? $image : $website . $image;
        }

       return null;
    }


    public function getGeo()
    {
        $address = $this->getAddress();

        $location = "";
        for ($x = 1; $x < count($address); $x++) {
            if (isset($address[$x])) $location .= $address[$x];
        }

        $geo = $this->geo->getGeocodeFromGoogle($location);

        return isset($geo->results[0]) ? $geo->results[0]->geometry->location : null;
    }


    public function getSlug()
    {
        return $this->slugger->set('attractions', 'slug_uri')->create($this->getName(), array(
            'family-fun', 'with-the-kids', 'good-times', 'fun-times'
        ));
    }


    public function getPhotos($website, $slug)
    {
        if (!$website) return null;

        $this->photoGrabber->set($website, $slug)->getAllImages();
    }

    /**
     * Read the description area and return array block of p tags
     * @return array
     */
    protected function scanDescription()
    {
        $blurb = array();
        $webpage = $this->crawler->filter('div.page_details')->html();
        $doc = new \DOMDocument();
        @$doc->loadHTML($this->clean($webpage));
        $description = $doc->getElementsByTagName('p');
        foreach ($description as $p) {
            if ($p->nodeValue) array_push($blurb, trim($p->nodeValue));
        }
        return $blurb;
    }


    protected function clean($text){
       return iconv("UTF-8", "ISO-8859-1//IGNORE", $text);
    }
}