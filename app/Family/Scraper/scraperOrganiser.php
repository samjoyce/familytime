<?php

namespace Family\Scraper;

use AnthonyMartin\GeoLocation\GeoLocation;
use Symfony\Component\DomCrawler\Crawler;


class ScraperOrganiser
{

    public function __construct()
    {
        $this->crawler = new Crawler();
    }

    /**
     * Scrape the next age in the databse
     * @return array|bool|null
     */
    public function scrapeNextPage()
    {
        $scrapePage = $this->nextPageToScrape();
        if($scrapePage == false) return 'completed'; //no more pages to scrape

        $this->updatePageScraped($scrapePage->id);

        $attraction = new scraperDayOutWithTheKids();

        $scrape = ($attraction->set($scrapePage->page)) ? $attraction->getAllVenueData()->asArray() : null;
        $scrape["category_id"] = $scrapePage->category;

        if ($scrape) {
            $venue = \Attraction::create($scrape);

            return $scrape;
        }

        return false;
    }

    /**
     * searches the preprogrmed websites to have their indexs scraped and indexed based on location
     * @param $location
     */
    public function searchWeb($location)
    {
        $location = str_replace('', '_', $location);
        $this->indexSite('http://www.dayoutwiththekids.co.uk/things-to-do/' . $location);
        $this->indexSite('http://www.dayoutwiththekids.co.uk/things-to-do/' . $location .'?page=2');
        $this->indexSite('http://www.dayoutwiththekids.co.uk/things-to-do/' . $location .'?page=3');
        $this->indexSite('http://www.dayoutwiththekids.co.uk/things-to-do/' . $location .'?page=4');
    }

    /**
     * returns the page object to be scraped next
     * @return mixed
     */
    public function nextPageToScrape()
    {
        $pageToScrape = \Scrape::where('last_scraped', '0000-00-00')->take(1)->get();
        return isset($pageToScrape) ? $pageToScrape[0] : false;
    }

    /**
     * this updates the scrape date when updates
     * @param $id
     */
    public function updatePageScraped($id)
    {
        $pageToSave = \Scrape::find($id);
        $pageToSave->update(array("last_scraped" => Date('Y-m-d')));
    }


    /**
     * add found web address to the database if not aready in db
     * @param $websiteListPage
     */
    public function indexSite($websiteListPage)
    {
        $list = $this->getListOfPages($websiteListPage);
        $pagesToScrape = $list[0];
        $numberInserted = 0;


        #add pages to scrape into databse if not in
        for ($x = 0; $x < count($pagesToScrape); $x++) {
            $exists = \DB::table('scrapes')->where('page', $pagesToScrape[$x]['url'])->first();
            if (!$exists) {
                $url = $pagesToScrape[$x]['url'];
                $category = $pagesToScrape[$x]['category'];
                if($url != "") {
                    \DB::statement("insert into scrapes (page, category) values ('$url', '$category')");
                    $numberInserted++;
                }
            }
        }

        return ($numberInserted > 0) ? true : false;
    }


    /**
     * This gets a list of pges to scrape from the main website
     * @return array
     */
    public function getListOfPages($searchURL)
    {
        $this->indexScrape = "http://www.dayoutwiththekids.co.uk";
        $dom = file_get_contents($searchURL);

        $this->crawler->addContent($dom);

        $listOfPages = $this->crawler->filter('div.result')->each(function($node) {
            $url = $node->filter('div.description')->each(function($link){
                    return $this->indexScrape . $link->filter('a')->attr('href');
                });

            $category = $node->filter('div.key')->each(function($link){
                return $this->convertToCategory($link->filter('img')->attr('src'));
            });

            if(isset($url[0]) && isset($category[0])) return array('url' => $url[0], 'category' => $category[0]);

            return null;
        });


        return array($listOfPages);
    }


    //0 - active, 1 - playcenter, 2 - animal, 3 - educational, 4 - water, 5 - indoors, 6 - creative
    protected function convertToCategory($string){
        $cats = array('activity' => 0,
           'playcentre' => 1,
           'animal' => 2,
           'history' => 3,
           'farms' => 2,
           'horseriding' => 0,
           'water' => 4,
           'karting' => 0,
           'outdoors' => 0,
           'bowling' => 5,
           'artistic' => 6,
           'museum' => 3,
           'ski_slope' => 0,
           'theme' => 0,
           'skating' => 0,
           'beeches' => 4,
           'transport' => 3,

        );

        foreach($cats as $key => $value){
            if(strpos($string, $key)) return $value;
        }

        return null;
    }
}