<?php
/**
 * Created by PhpStorm.
 * User: Sam
 * Date: 24/05/2014
 * Time: 18:26
 */
namespace Family\Scraper;

interface scraperInterface
{
    /**
     * Set the instance to the requested html page
     * @param $page
     */
    public function set($page);

    /**
     * Get the contact details to be used to get name, address and telephone
     * @return array
     */
    public function contactDetails();

    public function getAllVenueData();

    public function asArray();

    /**
     * @return array
     */
    public function getAddress();

    /**
     * gets the venue name from the contct detals and removes
     * @param $contact
     * @return mixed
     */
    public function getName();

    /**
     * @param $contact
     */
    public function getTelephone();

    /**
     *
     */
    public function getEmail();

    /**
     * @return array
     */
    public function getDescription();

    /**
     * return website if last item in description
     * @param $description
     * @return string
     */
    public function getWebsite();

    /**
     * @return null|string
     */
    public function getAdmission();

    /**
     * @return null|string
     */
    public function getOpeningTimes();

    /**
     * Gets the url address of first image on venues website
     * @param $website
     * @return null|string
     */
    public function getLogo($website);

    public function getGeo();

    public function getSlug();
}