<?php

namespace Family\Email;

use Illuminate\Support\ServiceProvider;

class EmailServiceProvider extends ServiceProvider{

    public function register(){
        $this->app->bind('Family\Email\EmailInterface', 'Family\Email\MailgunRepository');
    }
    
}