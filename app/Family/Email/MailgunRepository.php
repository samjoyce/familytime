<?php
namespace Family\Email;

use Bogardo\Mailgun\Facades\Mailgun;

class MailgunRepository implements EmailInterface
{

    protected $superviseWarning = 'This message is not sent to admin, but was sent to someone else via Funbank.co.uk';

    public function send($view, $data, $meta, $admin = true){
        Mailgun::send($view, $data, function($message) use ($meta)
        {
            $message->to($meta["to"]);
            $message->from($meta["from"]);
            $message->subject($meta["subject"]);
            $message->replyTo($meta["from"]);
        });

        if($admin) {
            //also send to admin for monitoring, cancel at some point
            $data["userMessage"] = $this->superviseWarning . $data["userMessage"];
            Mailgun::send($view, $data, function ($message) use ($meta) {
                $message->to(\Config::get('admin.email'));
                $message->from('Message sent using Funbank.co.uk');
                $message->subject('Message sent using Funbank.co.uk');
            });
        }
    }

}