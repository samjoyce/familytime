<?php

namespace Family\Email;

interface EmailInterface
{
    public function send($view, $data, $meta, $admin = true);
}