<?php

namespace Family\Location;

use AnthonyMartin\GeoLocation\GeoLocation;
use Family\Weather\Weather;
use Illuminate\Support\Facades\Session;

class Location implements LocationInterface
{

    protected $location;

    public function __construct()
    {
        $this->location = Session::has('location') ? unserialize(Session::get('location')) : null;
        $this->ipLocation = new LocationByIp();
        $this->weather = new Weather();
    }

    /**
     * Set the required params for given location and store in session
     * @param $force allows overide to set api calls such as weather.
     * @param $conserveResource allows calls not to be made, such as bot request.
     * @param $location
     * @return \stdClass
     */
    public function setLocation($location, $conserveResource = false, $force = false)
    {
        $geo = GeoLocation::getGeocodeFromGoogle($location);

        //if no geo coordinates send false
        if(!isset($geo->results[0])) return false;

        if ($conserveResource == false) {
            if ($this->isLocationNew($geo) || $force == true) {
                $this->weather->setWeather($geo->results[0]->geometry->location->lat, $geo->results[0]->geometry->location->lng);
                $geo->weather = $this->weather;
                Session::put('location', serialize($geo));
            }
        }

        return $geo;
    }

    /**
     * Get remembered location
     * @return mixed
     */
    public function getLocation()
    {
        if (Session::has('location')) {
            return unserialize(Session::get('location'));
        }

        return false;
    }

    /**
     * This checks to compare location to stored location. This allows us to reduce api call usage.
     * @param $location
     * @return bool
     */
    public function isLocationNew($location)
    {
        $storedLocation = $this->getLocation();
        if ($storedLocation) {
            if ($storedLocation->results[0]->formatted_address == $location->results[0]->formatted_address) {
                return false;
            };
        }

        return true;
    }

    public function getLongitude()
    {
        $location = $this->getLocation();
        return $location->results[0]->geometry->location->lng;
    }

    public function getLatitude()
    {
        $location = $this->getLocation();
        return $location->results[0]->geometry->location->lat;
    }

}