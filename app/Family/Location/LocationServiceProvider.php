<?php

namespace Family\Location;

use Illuminate\Support\ServiceProvider;

class LocationServiceProvider extends ServiceProvider {

    public function register(){
        $this->app->bind('Family\Location\LocationInterface',
            'Family\Location\Location');
    }

} 