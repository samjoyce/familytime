<?php

namespace Family\Location;

use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Config;


class LocationByIp{

    public function __construct()
    {
        $this->ipKey = Config::get('keys.ipinfodb');
    }

    //get the location based on IP
    public function IpLocation($ip)
    {
        return file_get_contents('http://api.ipinfodb.com/v3/ip-city/?key=' . $this->ipKey . '&ip=' . $ip . '&format=json');
    }

    //get the location and remember
    public function rememberIPLocation($ip)
    {
        $location = $this->IpLocation($ip);
        Session::put('IpLocation', base64_encode(serialize($location)));

        return $location;
    }


    //get the remembered location or get new if not or error
    public function getIpLocation($ip = null)
    {
       if (Session::has('IpLocation')) {
           try {
               unserialize(base64_decode(Session::get('IpLocation')));
           }
           catch(\ErrorException $e){
               return $this->rememberIPLocation($ip);
           }
       }
        return $this->rememberIPLocation($ip);
    }

    //get the geo info as a json
    public function getIpLocationAsJson($ip = null)
    {
        return json_decode($this->getIpLocation($ip));
    }


    public function getDefaultSearch($ip)
    {
        $details = $this->IpLocation($ip);
        return $details->city . ', ' . $details->country;
    }
}