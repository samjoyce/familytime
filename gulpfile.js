var gulp = require('gulp');
var sass = require('gulp-ruby-sass');
var uglify = require('gulp-uglify');
var concat = require('gulp-concat');


var paths = {
    scripts: ['app/assets/js/priority/*.js',
        'app/assets/js/*.js',
        'bower_components/foundation/js/foundation.js',
        'bower_components/foundation/js/foundation/foundation-reveal.js',
        'bower_components/foundation/js/foundation/foundation-alert.js'],
    images: 'client/img/**/*',
    sass: ['app/assets/sass/*.scss', 'app/assets/sass/*', 'app/assets/sass/components/*']
};


gulp.task('sass', function() {
    return gulp.src('app/assets/sass/styles.scss')
        .pipe(sass({style: 'compressed'}))
        .pipe(gulp.dest('public/assets/css'));
});

gulp.task('scripts', function() {
    return gulp.src(paths.scripts)
        .pipe(concat('main.js'))
        .pipe(uglify())
        .pipe(gulp.dest('public/assets/js'));gulp
});

// Rerun the task when a file changes
gulp.task('watch', function() {
    gulp.watch(paths.sass, ['sass']);
});

// The default task (called when you run `gulp` from cli)
gulp.task('default', ['sass', 'watch']);

