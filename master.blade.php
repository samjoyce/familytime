<!DOCTYPE html>

<html>
<head>
    <link rel="shortcut icon" href="{{url('assets/images/favicon.ico')}}"/>
    @yield('title')

    @yield('meta')

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    {{HTML::style('assets/css/styles.css')}}

    @yield('styles')
</head>


<body>
@include('partials.header')

<div id="wrapper">
    @include('partials.modal')


    @yield('master-content')
</div>

@include('partials.footer')


//main script file for all pages
{{HTML::script("assets/js/main.js")}}

//scripts just for this page
@yield('scripts')

<script>
    //start foundation
   $(document).foundation();





</script>

</body>

</html>